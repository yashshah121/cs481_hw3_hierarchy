﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkingWithNavigation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RachelXaml : ContentPage
    {
        public RachelXaml()
        {
            InitializeComponent();
        }
        //OnAppearing Method is used for Dispalying Alert and opening a webview to play video
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await DisplayAlert("Fact", "I am gonna get one of those job things", "Continue");
            var browser = new WebView();
            browser.Source = "https://www.youtube.com/watch?v=YEOhtdk8UiY";
            Content = browser;

        }
        //This will give a alert message while poping back to the previous page.
        protected async override void OnDisappearing()
        {

            await DisplayAlert("Bye", "You are my lobster", "Exit");


        }

    }
}