﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkingWithNavigation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RossXaml : ContentPage
    {
        public RossXaml()
        {
            InitializeComponent();
        }
        //OnAppearing Method is used for Dispalying Alert and opening a webview to play video.
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await DisplayAlert("Fact", "Pivot Pivot Pivot ", "Continue");
            var browser = new WebView();
            browser.Source = "https://www.youtube.com/watch?v=GWV4e3_DBn8";
            Content = browser;

        }
        //This will give a alert message while poping back to the previous page.
        protected async override void OnDisappearing()
        {
            await DisplayAlert("Bye", "Unagi is total state of awarness", "Exit");
        }
    }
}