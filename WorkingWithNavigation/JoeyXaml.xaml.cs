﻿using System;
using Xamarin.Forms;

namespace WorkingWithNavigation
{
	public partial class JoeyXaml : ContentPage
	{
		public JoeyXaml ()
		{
			InitializeComponent ();
			
		}
		//OnAppearing Method is used for Dispalying Alert and opening a webview to play video
		protected async override void OnAppearing()
		{
			base.OnAppearing();
			await DisplayAlert("Fact", "Joey Doesn't share food", "Continue🌭🌭🌭");
			var browser = new WebView();
			browser.Source = "https://www.youtube.com/watch?v=YjQ1xD6UL-4";
			Content = browser;

		}
		//This will give a alert message while poping back to the previous page.
		protected async override void OnDisappearing()
		{
			
			await DisplayAlert("Bye", "Why God Why?", "Exit🌭🌭🌭");
			

		}



	}
}
