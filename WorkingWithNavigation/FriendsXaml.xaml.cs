﻿using System;
using Xamarin.Forms;

namespace WorkingWithNavigation
{
	public partial class FriendsXaml : ContentPage
	{
		public FriendsXaml ()
		{
			InitializeComponent ();
		}
		//All Click Buttons and there events are as follows. For all six pages.
		//This is a Navigation Page.
		// I have taken Refernce from this particular linkhttps://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/navigation/hierarchical
		async void OnjoeyClicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync (new JoeyXaml ());
		}
		async void OnphoebeClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new PhoebeXaml());
		}
		async void OnrossClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new RossXaml());
		}
		async void OnrachelClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new RachelXaml());
		}
		async void OnchandlerClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new ChandlerXaml());
		}
		async void OnmonicaClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new MonicaXaml());
		}
	}
}
