﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace WorkingWithNavigation
{
	public partial class PhoebeXaml : ContentPage
	{
		public PhoebeXaml ()
		{
			InitializeComponent ();
		}
		//OnAppearing Method is used for Dispalying Alert and opening a webview to play video
		protected async override void OnAppearing()
		{
			base.OnAppearing();
			await DisplayAlert("Fact", "Regina Phalange", "Continue🐱🐱🐱");
			var browser = new WebView();
			browser.Source = "https://www.youtube.com/watch?v=Hrk0HGM3c7k";
			Content = browser;

		}
		//This will give a alert message while poping back to the previous page.
		protected async override void OnDisappearing()
		{

			await DisplayAlert("Bye", "Come on Will take of your shirt and tell us. ", "Exit🌭🌭🌭");


		}


	}
}
